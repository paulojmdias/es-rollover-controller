package esutil

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestParseLegacyIndexTemplates(t *testing.T) {

	testCases := []struct {
		resp           string
		expectedOutput []string
	}{
		{
			resp:           `{"test_es-rollover-controller":{"order":0,"index_patterns":["test"],"settings":{},"mappings":{},"aliases":{}},"wasd_es-rollover-controller":{"order":0,"index_patterns":["wasd*"],"settings":{},"mappings":{},"aliases":{}}}`,
			expectedOutput: []string{"test_es-rollover-controller", "wasd_es-rollover-controller"},
		},
		{
			resp:           `{"another_es-rollover-controller":{"order":0,"index_patterns":["another*"],"settings":{},"mappings":{},"aliases":{}}}`,
			expectedOutput: []string{"another_es-rollover-controller"},
		},
	}

	for _, c := range testCases {
		actualOutput, err := parseLegacyIndexTemplates([]byte(c.resp))
		assert.NoError(t, err)
		assert.ElementsMatch(t, c.expectedOutput, actualOutput)
	}
}
