# Quickstart Guide

This quickstart example will showcase how to deploy a complete solution, including the controller, Elasticsearch and Filebeat to a Kubernetes cluster.
For a better illustration of the final result, you can referenc the [example scenario](../../../README.md/#example-scenario).

<!-- TOC -->

- [Setting up Elasticsearch](#setting-up-elasticsearch)
- [Deploy the Kibana](#deploy-the-kibana)
- [Deploying the Controller](#deploying-the-controller)
- [Deploying Filebeat](#deploying-filebeat)
- [Deploying a Demo Application to Generate Logs](#deploying-a-demo-application-to-generate-logs)
- [Reviewing the Results in Kibana](#reviewing-the-results-in-kibana)

<!-- /TOC -->

## Setting up Elasticsearch

Follow the [Official Elasticsearch Operator Quickstart](https://www.elastic.co/guide/en/cloud-on-k8s/current/k8s-quickstart.html#k8s-deploy-eck) to deploy the `quickstart-es` instance to the Kubernetes cluster.

After the instance is deployed, port-forward the Elasticsearch service to your local machine so we can add some custom settings:

```sh
kubectl port-forward service/quickstart-es-http 9200
PASSWORD=$(kubectl get secret quickstart-es-elastic-user -o=jsonpath='{.data.elastic}' | base64 --decode)
```

Now we can adjust the Cluster settings to disable automatic index creation for the write alias created by our controller:

```sh
curl -k  -u "elastic:$PASSWORD"  -XPUT "https://localhost:9200/_cluster/settings?pretty" -H 'Content-Type: application/json' -d'
{
    "persistent": {
        "action.auto_create_index": "-*_writealias,+*"
    }
}
'
```

This will prevent Filebeat from accidentally creating an index, when the controller did not yet create the matching write alias.

## Deploy the Kibana

Follow the [Official Elasticsearch Operator Quickstart](https://www.elastic.co/guide/en/cloud-on-k8s/current/k8s-quickstart.html#k8s-deploy-kibana) to deploy the `quickstart-kb` Kibana to the Kubernetes cluster.

This will allow us to review our logs and setup later.

## Deploying the Controller

Next we will deploy the es-rollover-controller to the Kubernetes cluster via the provided Helm chart. Also we will override some of the default values to connect to our quickstart-es instance created in the previous step.

Lets start by adding the Helm repository of the controller to our local setup:

```sh
helm repo add es-rollover-controller https://msvechla.gitlab.io/es-rollover-controller
```

As we want to configure the es-rollover-controller to connect to the `quickstart-es` elasticsearch instance, we have to overwrite some of the default values of the controller. Copy the [quickstart-es-values.yaml](quickstart-es-values.yaml) file to your local machine, which contains all necessary configuration.

For demo purposes we also want to rollover indices more quickly, this is why we also add a [custom_ilm_policy.yaml](custom_ilm_policy.yaml), referenced in our custom values file:

```sh
kubectl apply -f custom_ilm_policy.yaml
```

Finally let's delpoy the controller with our custom values:

```sh
helm install demo es-rollover-controller/es-rollover-controller -f quickstart-es-values.yaml
```

The controller is now deployed and watching all pods with the label `elastic-index` set.

## Deploying Filebeat

Next we have to deply Filebeat and configure it to forward all logs from pods with the `elastic-index` label attached to the matching write alias created by the controller.

An example all-in-one daemonset configuration of Filebeat which achieves this can be found at [filebeat.yaml](filebeat.yaml), so let's deploy this as well:

```sh
kubectl apply -f filebeat.yaml
```

Filebeat will now forward all logs from pods with the `elastic-index` label set to a desired key to the matching write alias inside the `quickstart-es` cluster.

## Deploying a Demo Application to Generate Logs

We will create a deployment `kube-testlog` from the [kube-testlog.yaml](kube-testlog.yaml), which will generate random json messages so we can test our setup. Additionally we specify the label `elastic-index: kube-testlog` on the deployment. This will tell es-rollover-controller to create the matching write alias, index template and rollover indices.

```sh
kubectl apply -f kube-testlog.yaml
```

The application will now start to produce random log messages.

## Reviewing the Results in Kibana

To get started, retrieve the password of the `elastic` user to access Kibana:

```sh
echo $(kubectl get secret quickstart-es-elastic-user -o=jsonpath='{.data.elastic}' | base64 --decode)
```

Then port-forward the Kibana service and open [https://localhost:5601](https://localhost:5601) in your browser to login:

```sh
kubectl port-forward service/quickstart-kb-http 5601
```

Navigate to the `Management / Index-Management` Tab to see all indices that have been setup. Over time you should see the following indices:

```txt
kube-testlog-000001
kube-testlog-000002
etc
```

By clicking on the latest rollover index, you should also see an Alias with the name `kube-testlog_writealias` configured.

Congratulations, the completely automated rollover setup is now complete.

Happy Indexing 🚀
